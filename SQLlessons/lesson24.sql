/*Перечислите номера моделей любых типов, имеющих самую высокую цену по всей имеющейся в базе данных продукции.
*/

WITH Price_Out AS ( 
SELECT model, price FROM PC
UNION ALL
SELECT model, price FROM Laptop
UNION ALL
SELECT model, price FROM Printer)
SELECT DISTINCT model FROM Price_Out WHERE price = (SELECT MAX(price) FROM Price_Out)