/*Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).*/
SELECT Laptop.model, price FROM Product JOIN Laptop ON Product.model = Laptop.model
WHERE maker = 'B' UNION SELECT PC.model, price FROM Product JOIN PC ON Product.model = PC.model
WHERE maker = 'B' UNION SELECT Printer.model, price FROM Product JOIN Printer ON Product.model = Printer.model
WHERE maker = 'B'
