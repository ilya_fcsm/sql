/*Используя таблицу Product, определить количество производителей, выпускающих по одной модели. */
WITH Table_name AS (
SELECT COUNT(model) AS qty FROM Product GROUP BY maker)
SELECT COUNT(qty) FROM Table_name WHERE qty = 1
