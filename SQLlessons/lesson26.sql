/*Найдите среднюю цену ПК и ПК-блокнотов, выпущенных производителем A (латинская буква). Вывести: одна общая средняя цена.*/
WITH Table_name AS (
SELECT price FROM PC WHERE model IN
(SELECT model FROM Product WHERE maker = 'A')
UNION ALL
SELECT price FROM Laptop WHERE model IN
(SELECT model FROM Product WHERE maker = 'A'))
SELECT AVG(price) FROM Table_name